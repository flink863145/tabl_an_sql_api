package org.example;

import org.apache.flink.table.api.Table;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple5;
import org.apache.flink.table.api.TableEnvironment;
import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.api.java.ExecutionEnvironment;

//import org.apache.flink.table.api.bridge.java.BatchTableEnvironment;


import org.apache.flink.table.api.*;

import static org.apache.flink.table.api.Expressions.*;



public class TableApiExample {

  public static void main(String[] args) throws Exception {

    EnvironmentSettings settings = EnvironmentSettings
            .newInstance()
            .inStreamingMode()
            .build();

    TableEnvironment tEnv = TableEnvironment.create(settings);



    final String tableDDL = "CREATE TEMPORARY TABLE CatalogTable (" +
      "date STRING, " +
      "month STRING, " +
      "category STRING, " +
      "product STRING, " +
      "profit INT " +
      ") WITH (" +
      "'connector' = 'filesystem', " +
      "'path' = 'file:///home/myvm/Flink/Case Study 2 - Bank Real-Time Fraud detection/Table and SQL API/src/main/resources/avg', " +
      "'format' = 'csv'" +
      ")";

    tEnv.executeSql(tableDDL);

    Table catalog = tEnv.from("CatalogTable");

    /* querying with Table API */
    Table table = catalog
      .filter($("category").isEqual("Category5"))
      .groupBy($("month"))
      .select($("month"), $("profit").sum().as("sum"))
      .orderBy($("sum"));




    table.execute().print();

  }

  public static class Row1 {
    public String month;
    public Integer sum;

    public Row1() {}

    public String toString() {
      return month + "," + sum;
    }
  }

}